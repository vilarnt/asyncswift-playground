//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

func downloadAllThumbnails(ofMovies movies: [Movie]) async throws -> [UIImage] {
    return try await withThrowingTaskGroup(of: UIImage.self) { taskGroup in
        // Create all asynchronous tasks first...
        for movie in movies {
            taskGroup.addTask { return try await service.getMovieThumbnail(movie: movie) }
        }

        // ...then build the resulting array over tasks' results
        var result = [UIImage]()
        while let thumbnail = try await taskGroup.next() {
            result.append(thumbnail)
        }
        return result
    }
}

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        let thumbnails = try await downloadAllThumbnails(ofMovies: movies)
        print("Done downloading all \(thumbnails.count) thumbnails.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
