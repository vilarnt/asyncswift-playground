//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1, maxSeconds: 1.5))


/*:
 here is the real magic: An implementation
 */
extension MovieService {
    @available(macOS 10.15, iOS 13.0, watchOS 6.0, tvOS 13.0, *)
    public func getAllThumbnails(of movies: [Movie]) -> AsyncStream<UIImage> {
        return AsyncStream(UIImage.self) { continuation in
            Task {
                try await withThrowingTaskGroup(of: Void.self) { taskGroup in
                    for movie in movies {
                        taskGroup.addTask {
                            continuation.yield(try await getMovieThumbnail(movie: movie))
                        }
                    }

                    try await taskGroup.waitForAll()
                    continuation.finish()
                }
            }
        }
    }
}


Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        for await thumbnail in service.getAllThumbnails(of: movies) {
            print("Thumbnail size = \(thumbnail.size)")
        }
        print("All thumbnails downloaded.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
