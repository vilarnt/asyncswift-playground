//: [Previous](@previous)

import Foundation

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        //: Get the thumbnails for the first 5 movies
        let t0 = try await service.getMovieThumbnail(movie: movies[0])
        let t1 = try await service.getMovieThumbnail(movie: movies[1])
        let t2 = try await service.getMovieThumbnail(movie: movies[2])
        let t3 = try await service.getMovieThumbnail(movie: movies[3])
        let t4 = try await service.getMovieThumbnail(movie: movies[4])

        let thumbnails = [t0, t1, t2, t3, t4]

        print("Done downloading the first 5 thumbnails.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
