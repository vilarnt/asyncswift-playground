//: [Previous](@previous)

import Foundation

func downloadMovies(completionHandler: @escaping ([Movie]?, Error?) -> Void) {
    let request = URLRequest(url: URL(string: "http://localhost:8000/index.json")!)

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data else {
            completionHandler(nil, error)
            return
        }

        do {
            let movies = try JSONDecoder().decode([Movie].self, from: data)
            completionHandler(movies, nil)
        } catch {
            completionHandler(nil, error)
        }
    }
    task.resume()
}


// {as07a}


// {as07b}
downloadMovies { movies, error in
    guard let movies = movies else {
        print("Error: \(error.debugDescription)")
        return
    }

    print("Downloaded \(movies.count) movies.")
}


//: [Next](@next)
