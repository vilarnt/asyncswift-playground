//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

func downloadAllThumbnails(ofMovies movies: [Movie]) async throws -> [UIImage] {
    // Create all asynchronous tasks first...
    var tasks = [Task<UIImage, Error>]()
    for movie in movies {
        let task = Task { return try await service.getMovieThumbnail(movie: movie) }
        tasks.append(task)
    }

    // ...then build the resulting array over tasks' results
    var thumbnails = [UIImage]()
    for task in tasks {
        thumbnails.append(try await task.value)
    }

    return thumbnails
}

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        let thumbnails = try await downloadAllThumbnails(ofMovies: movies)
        print("Done downloading all \(thumbnails.count) thumbnails.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
