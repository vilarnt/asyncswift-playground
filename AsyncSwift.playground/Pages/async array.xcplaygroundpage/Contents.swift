//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1, maxSeconds: 1.5))

/*:
 here is the real magic: An implementation
 */
extension MovieService {
    @available(macOS 10.15, iOS 13.0, watchOS 6.0, tvOS 13.0, *)
    public func getAllThumbnails(of movies: [Movie]) async throws -> [UIImage] {
        return try await withThrowingTaskGroup(of: UIImage.self) { taskGroup in
            for movie in movies {
                taskGroup.addTask { try await self.getMovieThumbnail(movie: movie) }
            }

            var result = [UIImage]()
            while let thumbnail = try await taskGroup.next() {
                result.append(thumbnail)
            }
            return result
        }
    }
}


Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        let thumbnails = try await service.getAllThumbnails(of: movies)
        print("All \(thumbnails.count) thumbnails downloaded.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
