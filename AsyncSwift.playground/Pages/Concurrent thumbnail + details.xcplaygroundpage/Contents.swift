//: [Previous](@previous)

import Foundation

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        // GOAL: Download both the thumbnail and the details of the first movie.
        // {as06a}

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
