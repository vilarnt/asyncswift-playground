//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        for movie in movies {
            Task {
                let thumbnail = try await service.getMovieThumbnail(movie: movie)
                print("Thumbnail for movie “\(movie.origTitle)” has size = \(thumbnail.size)")
            }
        }

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
