//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        async let thumbnail = service.getMovieThumbnail(movie: movies[0])
        async let details = service.getMovieDetails(movie: movies[0])

        print("Thumbnail's size = \(try await thumbnail.size), genres = \(try await details.genres.joined(separator: ", "))")
        print(try await details.description)

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
