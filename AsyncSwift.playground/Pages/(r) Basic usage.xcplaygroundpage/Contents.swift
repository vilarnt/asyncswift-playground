//: [Previous](@previous)

import Foundation

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1, maxSeconds: 1.5))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        let image = try await service.getMovieThumbnail(movie: movies[0])
        print("Image size: \(image.size)")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
