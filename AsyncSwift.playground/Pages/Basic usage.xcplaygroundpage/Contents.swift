import Foundation

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1, maxSeconds: 1.5))

// GOAL 1: Use service.listMovies() to fetch the list of movies and print the
// movies count.
// {as00a}

// GOAL 2: User service.getMovieThumbnil() to fetch the thumbnail of the first movie
// and print its size.
// {as00b}

//: [Next](@next)
