//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

func downloadAllThumbnails(ofMovies movies: [Movie]) async throws -> [UIImage] {
    return []  // TODO: - Implement the actual code {as04a}
}

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        let thumbnails = try await downloadAllThumbnails(ofMovies: movies)
        print("Done downloading all \(thumbnails.count) thumbnails.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
