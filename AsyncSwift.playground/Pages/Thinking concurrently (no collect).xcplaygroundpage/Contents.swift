//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        // GOAL: Iterate over all movies, call service.getMovieThumbnail()
        // on each one and print movie's title and thumbnail's size.
        // {as01, as02}

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
