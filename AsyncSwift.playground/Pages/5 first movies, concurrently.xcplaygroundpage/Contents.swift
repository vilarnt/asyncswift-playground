//: [Previous](@previous)

import Foundation

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        //: Get the thumbnails for the first 5 movies
        async let t0 = service.getMovieThumbnail(movie: movies[0])
        async let t1 = service.getMovieThumbnail(movie: movies[1])
        async let t2 = service.getMovieThumbnail(movie: movies[2])
        async let t3 = service.getMovieThumbnail(movie: movies[3])
        async let t4 = service.getMovieThumbnail(movie: movies[4])

        let thumbnails = [try await t0, try await t1, try await t2, try await t3, try await t4]

        print("Done downloading the first 5 thumbnails.")

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
