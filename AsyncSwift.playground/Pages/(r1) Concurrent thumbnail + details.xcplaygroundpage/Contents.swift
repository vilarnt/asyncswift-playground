//: [Previous](@previous)

import Foundation
import UIKit

//: Create the service, embedded in turtlelizer (to slow down requests) and logger middlewares
let service = LogMiddleware(wrapping: TurtlelizerMiddleware(wrapping: MovieAPIService(), minSeconds: 1.5, maxSeconds: 2))

Task {
    do {
        print("Starting download of movies list...")
        let movies = try await service.listMovies()
        print("There are \(movies.count) movies")

        let thumbnail = try await service.getMovieThumbnail(movie: movies[0])
        let details = try await service.getMovieDetails(movie: movies[0])

        print("Thumbnail's size = \(thumbnail.size), genres = \(details.genres.joined(separator: ", "))")
        print(details.description)

    } catch {
        print("An error occurred: \(error)")
    }
}

//: [Next](@next)
