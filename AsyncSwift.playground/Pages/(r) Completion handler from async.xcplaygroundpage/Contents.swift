//: [Previous](@previous)

import Foundation

func downloadMovies(completionHandler: @escaping ([Movie]?, Error?) -> Void) {
    let request = URLRequest(url: URL(string: "http://localhost:8000/index.json")!)

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data else {
            completionHandler(nil, error)
            return
        }

        do {
            let movies = try JSONDecoder().decode([Movie].self, from: data)
            completionHandler(movies, nil)
        } catch {
            completionHandler(nil, error)
        }
    }
    task.resume()
}


func downloadMovies() async throws -> [Movie] {
    try await withCheckedThrowingContinuation { continuation in
        downloadMovies { movies, error in
            guard let movies = movies else {
                continuation.resume(throwing: error!)
                return
            }
            continuation.resume(returning: movies)
        }
    }
}


Task {
    do {
        let movies = try await downloadMovies()
        print("Downloaded \(movies.count) movies.")
    } catch {
        print("Error: \(error)")
    }
}


//: [Next](@next)
