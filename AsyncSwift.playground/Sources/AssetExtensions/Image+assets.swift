//
//  Image+assets.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 26/10/22.
//

import Foundation
import SwiftUI


extension Image {
    public static let posterFailed = Image("poster-failed")
    public static let posterPlaceholder = Image("poster-placeholder")
    public static let thumbnailFailed = Image("thumbnail-failed")
    public static let thumbnailPlaceholder = Image("thumbnail-placeholder")

    #if DEBUG
    public static let mockupPoster = Image("mockup-poster")
    public static let mockupThumbnail = Image("mockup-thumbnail")
    #endif
}
