//
//  UIImage+assets.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 02/11/22.
//

import Foundation
import UIKit


extension UIImage {
    public static let posterFailed = UIImage(named: "poster-failed")!
    public static let posterPlaceholder = UIImage(named: "poster-placeholder")!
    public static let thumbnailFailed = UIImage(named: "thumbnail-failed")!
    public static let thumbnailPlaceholder = UIImage(named: "thumbnail-placeholder")!

    #if DEBUG
    public static let mockupPoster = UIImage(named: "mockup-poster")!
    public static let mockupThumbnail = UIImage(named: "mockup-thumbnail")!
    #endif
}
