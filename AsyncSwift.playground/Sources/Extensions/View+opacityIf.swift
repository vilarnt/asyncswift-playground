//
//  View+opacityIf.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 29/10/22.
//

import SwiftUI


extension View {
    /**
     Apply an opacity if a given predicate is true, otherwise return this view unchanged.
     */
    @ViewBuilder public func opacity(if predicate: Bool, _ opacity: Double) -> some View {
        if predicate {
            self.opacity(opacity)
        } else {
            self
        }
    }
}
