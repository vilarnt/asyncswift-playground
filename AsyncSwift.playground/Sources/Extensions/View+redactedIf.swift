//
//  View+redactedIf.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 27/10/22.
//

import SwiftUI


extension View {
    /**
     Redact this view if a given predicate is true, otherwise return this view unchanged.
     */
    @ViewBuilder public func redacted(if predicate: Bool, reason: RedactionReasons = .placeholder) -> some View {
        if predicate {
            self.redacted(reason: reason)
        } else {
            self
        }
    }
}
