//
//  ImageURL+getURLForMainScreenScale.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 29/10/22.
//

import Foundation
import UIKit


extension Sequence where Element == ImageURL {
    /**
     Select the URL for a given scale. This is the greatest URL associated with a scale that is less than or equal the
     input argument.
     */
    public func getURLForScale(_ targetScale: CGFloat) -> ImageURL? {
        var selectedImageURL: ImageURL?

        for item in self {
            if item.scale > targetScale {
                continue
            }
            if selectedImageURL == nil || item.scale > selectedImageURL!.scale {
                selectedImageURL = item
            }
        }

        return selectedImageURL
    }

    /**
     Select the best URL for the main screen. This is the greatest URL associated with a scale that is less than or
     equal the main screen's scale.
     */
    public func getURLForMainScreenScale() -> ImageURL? {
        let scale = UIScreen.main.scale
        return getURLForScale(scale)
    }
}
