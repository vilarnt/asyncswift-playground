//
//  TurtlelizerMiddleware.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 03/11/22.
//

import Foundation
import UIKit


/**
 A middleware that artificially causes all operations to sleep for a random time, intended to emulate the effects of
 network latency for debugging/demonstration purposes.
 */
public actor TurtlelizerMiddleware: MovieService {
    public func listMovies() async throws -> [Movie] {
        await turtlelize()
        return try await wrappedService.listMovies()
    }

    public func getMovieThumbnail(movie: Movie) async throws -> UIImage {
        await turtlelize()
        return try await wrappedService.getMovieThumbnail(movie: movie)
    }

    public func getMovieDetails(movie: Movie) async throws -> MovieDetails {
        await turtlelize()
        return try await wrappedService.getMovieDetails(movie: movie)
    }

    public func getMoviePoster(movieDetails: MovieDetails) async throws -> UIImage {
        await turtlelize()
        return try await wrappedService.getMoviePoster(movieDetails: movieDetails)
    }

    private let wrappedService: MovieService
    private var minNanoseconds: UInt64
    private var maxNanoseconds: UInt64

    public init(wrapping wrappedService: MovieService, minSeconds: Double, maxSeconds: Double) {
        self.wrappedService = wrappedService
        self.minNanoseconds = UInt64(minSeconds * 1_000_000_000)
        self.maxNanoseconds = UInt64(maxSeconds * 1_000_000_000)
    }

    private func turtlelize() async {
        let sleepNanoseconds = UInt64.random(in: minNanoseconds...maxNanoseconds)
        if sleepNanoseconds > 0 {
            try? await Task.sleep(nanoseconds: sleepNanoseconds)
        }
    }
}
