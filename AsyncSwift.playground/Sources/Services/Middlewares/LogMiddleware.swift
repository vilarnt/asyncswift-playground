//
//  LogMiddleware.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 03/11/22.
//

import Foundation
import UIKit


/**
 A middleware that logs the start and end of all operations.
 */
public actor LogMiddleware: MovieService {
    public func listMovies() async throws -> [Movie] {
        return try await logAndExecute(subject: "listMovies()") {
            try await wrappedService.listMovies()
        }
    }

    public func getMovieThumbnail(movie: Movie) async throws -> UIImage {
        return try await logAndExecute(subject: "getMovieThumbnail(), movie.id = \(movie.id)") {
            try await wrappedService.getMovieThumbnail(movie: movie)
        }
    }

    public func getMovieDetails(movie: Movie) async throws -> MovieDetails {
        return try await logAndExecute(subject: "getMovieDetails(), movie.id = \(movie.id)") {
            try await wrappedService.getMovieDetails(movie: movie)
        }
    }

    public func getMoviePoster(movieDetails: MovieDetails) async throws -> UIImage {
        return try await logAndExecute(subject: "getMoviePoster(), movie.id = \(movieDetails.id)") {
            try await wrappedService.getMoviePoster(movieDetails: movieDetails)
        }
    }

    private let wrappedService: MovieService

    public init(wrapping wrappedService: MovieService) {
        self.wrappedService = wrappedService
    }

    private func logAndExecute<Data>(subject: String, callback: () async throws -> Data) async throws -> Data {
        print("Start \(subject)")
        let result = try await callback()
        print("End \(subject)")
        return result
    }
}
