//
//  MovieAPIService.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 26/10/22.
//

import os
import Foundation
import CoreGraphics
import UIKit
import SwiftUI


public enum MovieAPIServiceError: Error {
    case undecodableImage
    case noImageForScale
}


/**
 The API client implementation of `MovieService` that fetches data from a HTTP server.
 */
public class MovieAPIService {
    fileprivate static let baseURL = URL(string: "http://localhost:8000/index.json")!
    fileprivate static let urlSession = URLSession(configuration: .default)

    public init() { }

    fileprivate func buildURLRequest(from url: URL) -> URLRequest {
        var request = URLRequest(url: URL(string: url.path, relativeTo: Self.baseURL)!)
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        return request
    }

    fileprivate func getImage(from imageURL: ImageURL) async throws -> UIImage {
        let request = self.buildURLRequest(from: imageURL.url)
        let (data, _) = try await Self.urlSession.data(for: request)
        guard let image = UIImage(data: data, scale: imageURL.scale) else {
            throw MovieAPIServiceError.undecodableImage
        }
        return image
    }

    fileprivate func getImage(from imageURLs: [ImageURL]) async throws -> UIImage {
        guard let imageURL = imageURLs.getURLForMainScreenScale() else {
            throw MovieAPIServiceError.noImageForScale
        }
        return try await getImage(from: imageURL)
    }
}


extension MovieAPIService: MovieService {
    private static let logger = Logger()

    private func getAndDecode<Data>(url: URL) async throws -> Data where Data: Decodable {
        let request = self.buildURLRequest(from: url)
        let (rawData, _) = try await Self.urlSession.data(for: request)
        let decodedData = try JSONDecoder().decode(Data.self, from: rawData)
        return decodedData
    }

    public func listMovies() async throws -> [Movie] {
        return try await self.getAndDecode(url: Self.baseURL)
    }

    public func getMovieThumbnail(movie: Movie) async throws -> UIImage {
        return try await getImage(from: movie.thumbnail)
    }

    public func getMovieDetails(movie: Movie) async throws -> MovieDetails {
        return try await self.getAndDecode(url: movie.detailsUrl)
    }

    public func getMoviePoster(movieDetails: MovieDetails) async throws -> UIImage {
        return try await getImage(from: movieDetails.poster)
    }
}
