//
//  MockupMovieService.swift
//  asyncswift
//
//  Created by Vilar da Camara Neto on 25/10/22.
//

import Foundation
import UIKit


#if DEBUG

enum MockupServiceError: Error {
    case mockupFailure
}

/**
 A mockup implementation of `MovieService` for the purpose of preview rendering.
 */
public class MockupMovieService: MovieService {
    public enum MockupMode {
        case normal
        case keepLoading
        case alwaysFails
    }

    public static let mockupMovieWithFullData = Movie(id: "1", origTitle: "Back Without Forth", altTitle: "A Volta dos que Não Foram", year: 2022, duration: 123, detailsUrl: URL(string: "/1/index.json")!, thumbnail: [.init(scale: 1, url: URL(string: "/1/thumbnail.jpg")!)])
    public static let mockupMovieWithoutAltTitle = Movie(id: "2", origTitle: "CBL in a Nutshell", altTitle: nil, year: 2022, duration: 45, detailsUrl: URL(string: "/1/index.json")!, thumbnail: [.init(scale: 1, url: URL(string: "/2/thumbnail.jpg")!)])
    public static let mockupMovieWithoutYear = Movie(id: "3", origTitle: "Week", altTitle: "Sete Dias e Sete Noites", year: nil, duration: 100, detailsUrl: URL(string: "/3/index.json")!, thumbnail: [.init(scale: 1, url: URL(string: "/3/thumbnail.jpg")!)])
    public static let mockupMovieWithoutDuration = Movie(id: "4", origTitle: "Original Title", altTitle: "Título Original", year: 2022, duration: nil, detailsUrl: URL(string: "/4/index.json")!, thumbnail: [.init(scale: 1, url: URL(string: "/4/thumbnail.jpg")!)])
    public static let mockupMovieWithoutYearNorDuration = Movie(id: "5", origTitle: "The Sidewalk", altTitle: "Uma Trilha que Não Se Apaga Depois da Nevasca de Ontem", year: nil, duration: nil, detailsUrl: URL(string: "/5/index.json")!, thumbnail: [.init(scale: 1, url: URL(string: "/5/thumbnail.jpg")!)])

    private static let mockupMovies: [Movie] = [
        mockupMovieWithFullData,
        mockupMovieWithoutAltTitle,
        mockupMovieWithoutYear,
        mockupMovieWithoutDuration,
        mockupMovieWithoutYearNorDuration,
    ]

    public static let mockupMovieDetails = MovieDetails(id: "1", description: "Era uma vez um velho burufelho samaracutelho bussanfelho casado com uma velha burufelha samaracutelha bussanfelha.", imdbRating: 7.5, genres: ["Fantasy", "Horror", "Drama"], actors: ["Velho Samaracutelho", "Velha Samaracutelha"], directors: ["Gato Burufaco"], poster: [.init(scale: 1, url: URL(string: "/1/poster.jpg")!)])

    private let mode: MockupMode

    public init(mode: MockupMode = .normal) {
        self.mode = mode
    }

    private func getData<Data>(valueIfLoaded: () -> Data) -> AsyncData<Data> {
        switch mode {
        case .normal:
            return .loaded(valueIfLoaded())
        case .keepLoading:
            return .loading
        case .alwaysFails:
            return .failed
        }
    }

    private func getData<Data>(valueIfLoaded: () -> Data) async throws -> Data {
        switch mode {
        case .normal:
            return valueIfLoaded()
        case .keepLoading:
            // Calling withCheckedContinuation without calling continuation's .resume() will wait forever
            return await withCheckedContinuation { _ in }
        case .alwaysFails:
            throw MockupServiceError.mockupFailure
        }
    }

    public func listMovies() async throws -> [Movie] {
        return try await getData { Self.mockupMovies }
    }

    public func getMovieThumbnail(movie: Movie) async throws -> UIImage {
        return try await getData { .mockupThumbnail }
    }

    public func getMovieDetails(movie: Movie) async throws -> MovieDetails {
        return try await getData { Self.mockupMovieDetails }
    }

    public func getMoviePoster(movieDetails: MovieDetails) async throws -> UIImage {
        return try await getData { .mockupPoster }
    }
}

#endif
