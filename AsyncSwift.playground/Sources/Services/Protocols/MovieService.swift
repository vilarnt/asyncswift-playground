//
//  MovieService.swift
//  asyncswift
//
//  Created by Vilar da Camara Neto on 25/10/22.
//

import Foundation
import UIKit


/**
 A protocol for movie services.
 */
public protocol MovieService {
    /**
     List the 100 Most Popular Movies.
     */
    func listMovies() async throws -> [Movie]

    /**
     Retrieve the thumbnail image for a given movie.

     - Parameters:
       - movie: The movie for which the thumbnail is to be retrieved.
    */
    func getMovieThumbnail(movie: Movie) async throws -> UIImage

    /**
     Retrieve details about a given movie.

     - Parameters:
       - movie: The movie for which details are to be retrieved.
     */
    func getMovieDetails(movie: Movie) async throws -> MovieDetails

    /**
     Retrieve the poster image for a given movie.

     - Parameters:
       - movieDetails: The details of the movie for which the poster is to be retrieved.
    */
    func getMoviePoster(movieDetails: MovieDetails) async throws -> UIImage
}
