//
//  AsyncData.swift
//  asyncswift
//
//  Created by Vilar da Camara Neto on 25/10/22.
//

import Foundation


public enum AsyncData<Data> {
    /// Signals that the data is in process of loading.
    case loading
    /// Signals that the data was loaded and is ready.
    case loaded(Data)
    /// Signals that the data failed to load.
    case failed

    public var isLoaded: Bool {
        switch self {
        case .loading, .failed:
            return false
        case .loaded(_):
            return true
        }
    }

    var loadedData: Data? {
        switch self {
        case .loading, .failed:
            return nil
        case .loaded(let data):
            return .some(data)
        }
    }

    /**
     Apply a transformation function to the wrapped data, preserving the current status (loading / loaded / failed).

     - Parameters:
     - transform: The transformation function that receives the wrapped data and returns the intended transformed data
       to be wrapped. It is not called if the current status is not `.loaded`.
     - Returns: If the current status is `.loaded`, then the result is the result of the transformation wrapped as
       `.loaded` as well; otherwise, the result is a new `AsyncData` that preserves the current status (`.loading` or
       `.failed`) and has the same wrapped type of the transformation function.
     */
    func map<NewData>(_ transform: (Data) throws -> NewData) rethrows -> AsyncData<NewData> {
        switch self {
        case .loading:
            return .loading
        case .failed:
            return .failed
        case .loaded(let data):
            return .loaded(try transform(data))
        }
    }

    /**
     Apply a transformation function to the wrapped data, possibly changing the status (if loaded).

     - Parameters:
       - transform: The transformation function that receives the wrapped data and returns the resulting `AsyncData`
         instance. It is not called if the current status is not `.loaded`.
     - Returns: If the current status is `.loaded`, then the result is exactly the result of the transformation;
       otherwise, the result is a new `AsyncData` that preserves the current status (`.loading` or `.failed`) and has
       the same wrapped type of the transformation function.
     */
    func map<NewData>(_ transform: (Data) throws -> AsyncData<NewData>) rethrows -> AsyncData<NewData> {
        switch self {
        case .loading:
            return .loading
        case .failed:
            return .failed
        case .loaded(let data):
            return try transform(data)
        }
    }

    /**
     Asynchronously apply a transformation function to the wrapped data, preserving the current status (loading /
     loaded / failed).

     - Parameters:
     - transform: The asynchronous transformation function that receives the wrapped data and returns the intended
       transformed data to be wrapped. It is not called if the current status is not `.loaded`.
     - Returns: If the current status is `.loaded`, then the result is the result of the transformation wrapped as
       `.loaded` as well; otherwise, the result is a new `AsyncData` that preserves the current status (`.loading` or
       `.failed`) and has the same wrapped type of the transformation function.
     */
    func asyncMap<NewData>(_ transform: (Data) async throws -> NewData) async rethrows -> AsyncData<NewData> {
        switch self {
        case .loading:
            return .loading
        case .failed:
            return .failed
        case .loaded(let data):
            return .loaded(try await transform(data))
        }
    }

    /**
     Asynchronously apply a transformation function to the wrapped data, possibly changing the status (if loaded).

     - Parameters:
       - transform: The asynchronous transformation function that receives the wrapped data and returns the resulting
       `AsyncData` instance. It is not called if the current status is not `.loaded`.
     - Returns: If the current status is `.loaded`, then the result is exactly the result of the transformation;
       otherwise, the result is a new `AsyncData` that preserves the current status (`.loading` or `.failed`) and has
       the same wrapped type of the transformation function.
     */
    func asyncMap<NewData>(_ transform: (Data) async throws -> AsyncData<NewData>) async rethrows -> AsyncData<NewData> {
        switch self {
        case .loading:
            return .loading
        case .failed:
            return .failed
        case .loaded(let data):
            return try await transform(data)
        }
    }
}

extension AsyncData: Equatable where Data: Equatable { }

extension AsyncData: Hashable where Data: Hashable { }

extension AsyncData: CustomDebugStringConvertible {
    public var debugDescription: String {
        switch self {
        case .loading:
            return "AsyncData.loading"
        case .loaded(let data):
            if let data = data as? CustomDebugStringConvertible {
                return "AsyncData.loaded(\(data.debugDescription))"
            } else {
                return "AsyncData.loaded"
            }
        case .failed:
            return "AsyncData.failed"
        }
    }
}
