//
//  Movie.swift
//  asyncswift
//
//  Created by Vilar da Camara Neto on 25/10/22.
//

import Foundation


/**
 General information associated with a movie.
 */
public struct Movie: Identifiable, Decodable {
    public let id: String
    public let origTitle: String
    public let altTitle: String?
    public let year: Int?
    public let duration: Int?
    public let detailsUrl: URL
    public let thumbnail: [ImageURL]

    public init(id: String, origTitle: String, altTitle: String? = nil, year: Int? = nil, duration: Int? = nil, detailsUrl: URL, thumbnail: [ImageURL]) {
        self.id = id
        self.origTitle = origTitle
        self.altTitle = altTitle
        self.year = year
        self.duration = duration
        self.detailsUrl = detailsUrl
        self.thumbnail = thumbnail
    }
}
