//
//  ImageURL.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 29/10/22.
//

import Foundation


/**
 Store the (relative) URL of an image for a given screen scale
 */
public struct ImageURL: Decodable {
    public let scale: CGFloat
    public let url: URL
}
