//
//  MovieDetails.swift
//  AsyncSwift
//
//  Created by Vilar da Camara Neto on 27/10/22.
//

import Foundation


/**
 Detailed information associated with a movie.
 */
public struct MovieDetails: Identifiable, Decodable {
    public let id: String
    public let description: String
    public let imdbRating: Float?
    public let genres: [String]
    public let actors: [String]
    public let directors: [String]
    public let poster: [ImageURL]

    public init(id: String, description: String, imdbRating: Float? = nil, genres: [String], actors: [String], directors: [String], poster: [ImageURL]) {
        self.id = id
        self.description = description
        self.imdbRating = imdbRating
        self.genres = genres
        self.actors = actors
        self.directors = directors
        self.poster = poster
    }
}
